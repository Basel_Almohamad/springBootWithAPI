package com.SB.model;

import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;

public class ErrorResponse {

	private String message;
	private HttpStatus statusCode;

	private Date date;
	private List<String> detailsList;

	public ErrorResponse() {
		super();
	}
	public ErrorResponse(String message, HttpStatus statusCode, List<String> detailsList) {
		super();
		this.message = message;
		this.statusCode= statusCode;
		this.detailsList = detailsList;
		this.date=new Date();
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public HttpStatus getStatusCodeString() {
		return statusCode;
	}
	public void setStatusCodeString(HttpStatus statusCodeString) {
		this.statusCode= statusCodeString;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public List<String> getDetailsList() {
		return detailsList;
	}
	public void setDetailsList(List<String> detailsList) {
		this.detailsList = detailsList;
	}

}
