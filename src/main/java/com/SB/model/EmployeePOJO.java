package com.SB.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import javax.validation.constraints.*;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.UniqueElements;;

@Entity
@Table(name = "employees")
public class EmployeePOJO {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@ApiModelProperty(notes = "The databasse generated Employee ID")// add describtion to colum in DB
	private int id;

	@Column(name = "first_name")
	@ApiModelProperty(notes = "The databasse generated Employee Name",required = true)
	@NotEmpty(message="firstName must be empty")//Not empty
	private String firstName;

	@NotEmpty(message="lastName must be empty")//Not empty
	@Column(name = "last_name")
	private String lastName;

	@Email(message = "you should enter a valid mail")//valid mail
	@Column(name="email")
	private String email;
	//@NotEmpty(message="Salary must be empty")
	@Column(name = "Salary")
	private int salary;

	@Column(name = "phone" ,unique=true)
	@Min(value=11,message = "phone number must be 11 number")
	private long phoneNumber;
	@ManyToOne(cascade = CascadeType.ALL)
	private Department deparment;

	public EmployeePOJO(int id, String firstName, String lastNameString, int salary, Department deparment,String mail,long phone) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastNameString;
		this.salary = salary;
		this.deparment= deparment;
		this.phoneNumber=phone;
		this.email=mail;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public EmployeePOJO() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastNameString() {
		return lastName;
	}

	public void setLastNameString(String lastName) {
		this.lastName = lastName;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Department getDeparment() {
		return deparment;
	}

	public void setDeparment(Department deparment) {
		this.deparment = deparment;
	}

}