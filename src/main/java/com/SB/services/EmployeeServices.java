package com.SB.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.SB.Repositrys.EmployeeRepositry;
import com.SB.model.Department;
import com.SB.model.EmployeePOJO;

@Service
public class EmployeeServices {

	@Autowired // hier we did object injection
	private EmployeeRepositry repositry;

	public List<EmployeePOJO> getAllEmpoyees() {

		return repositry.findAll();

	}


	public EmployeePOJO getEmpByID(int id) {

		return repositry.findById(id).orElseThrow();

	}

	public List<EmployeePOJO> getEmpByFirstName(String firstName) {

		return repositry.findByFirstNameIgnoreCase(firstName);

	}

	public void saveEmployee(EmployeePOJO emp) {

		repositry.save(emp);

	}

	public void updateEmployee(EmployeePOJO emp) {

		repositry.save(emp);

	}

	public void deleteEmployee(int id) {

		repositry.deleteById(id);

	}

	//**********************************************//


	//API Master(get all employees in a specific department through the FK
	public List<EmployeePOJO> getAllEmpoyeesInDepartment(Department deparment) {

	//	return repositry.findByDepartment(deparment);
return null;
	}

}
