package com.SB.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.SB.Repositrys.DepartmentRepositry;
import com.SB.model.Department;

@Service
public class DepartmentServices {

	@Autowired
	private DepartmentRepositry departmentRepositry;

	public List<Department> getAllDepts(){
		return departmentRepositry.findAll();


	}

	public void addDep(Department dep) {

		 departmentRepositry.save(dep);
	}

	public void updateDep(Department dep) {

		departmentRepositry.save(dep);

	}
	public void deleteDep(int id) {

		departmentRepositry.deleteById(id);
	}
}
