package com.SB.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.SB.model.Department;
import com.SB.model.EmployeePOJO;
import com.SB.services.EmployeeServices;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * This class represents the implementation of services from repository
 *
 * @author baselalmohamad
 *
 */

@RestController
@RequestMapping("/api/v1")
@Api(value = "employees", description = "Opreation for employee services")//swagger annotion
public class EmployeeController {

	//********************application.properties******************//
	@Value("anyPath")
	// @Value("${pathName}")
	String pathName;
	@Value("spring.application.name")
	String appName;

	//******************** Object injection ******************//
	@Autowired
	private EmployeeServices service;



	@GetMapping("/employees") // getAll
	@ApiOperation(value="view a list of avialble employees",response = EmployeePOJO.class)
	@ApiResponses(value= {

			@ApiResponse(code = 200,message = "Successful retrieved list"),
			@ApiResponse(code = 401,message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403,message = "Accessing the resource your were trying to reach is forbidden"),
			@ApiResponse(code = 404,message = "The resource you were trying to reach is not found")
	})
	public List<EmployeePOJO> getAllEmployees() {
		System.out.println(pathName);
		System.out.println(appName);
		return service.getAllEmpoyees();

	}

	@GetMapping("/getempbyid")
	public EmployeePOJO getEmpById(@RequestParam int id) {

		return service.getEmpByID(id);

	}

	@GetMapping("/getempbyname")
	public List<EmployeePOJO> getEmpByfirstName(@RequestParam String firstName) {

		return service.getEmpByFirstName(firstName);

	}

	@PostMapping("/employees") // ***** insert with Validation*****//
	public ResponseEntity<EmployeePOJO> saveEmp(@Valid @RequestBody EmployeePOJO emp) {//first check the validation then go to the service

		service.saveEmployee(emp);

		return new ResponseEntity<EmployeePOJO>(emp ,HttpStatus.OK);
	}


//	@PostMapping("/employees") // ***** insert without validation*****//
//	public String saveEmp(@RequestBody EmployeePOJO emp) {
//		service.saveEmployee(emp);
//
//		return "saved successful";
//	}

	@PutMapping("/employees") // update
	public String updateEmp(@RequestBody EmployeePOJO emp) {

		service.updateEmployee(emp);
		;

		return "updated successful";
	}

	@DeleteMapping("/employees") // delete
	public String deleteEmp(@RequestParam int id) {

		service.deleteEmployee(id);
		;

		return "deleted successful";
	}

	// ************************************************//

	@GetMapping("/departments/{depId}/employees")
	public List<EmployeePOJO> getAllEmployeesInDep(@PathVariable int depId) {
		Department department = new Department();

		department.setId(depId);
		return service.getAllEmpoyeesInDepartment(department);
	}

}
