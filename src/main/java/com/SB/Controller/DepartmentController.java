package com.SB.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.SB.model.Department;
import com.SB.services.DepartmentServices;

@RestController// tell sprint that this is a rest controller
@RequestMapping("/api/v1")
public class DepartmentController {

	@Autowired
	private DepartmentServices departmentServices;


	@GetMapping("/departments")
	public List<Department> getAllDepts(){
		return departmentServices.getAllDepts();


	}
	@PostMapping("/departments")
	public void addDep(@RequestBody Department dep) {

		departmentServices.addDep(dep);;
	}
	@PutMapping("/departments")
	public void updateDep(@RequestParam Department dep) {

		departmentServices.updateDep(dep);;

	}
	@DeleteMapping("/departments")
	public void deleteDep(@RequestParam int id) {

		departmentServices.deleteDep(id);;
	}



}
