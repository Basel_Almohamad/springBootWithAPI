package com.SB.base;


import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
/**
 * Add Describtion to any Colum or opreation even though to Controller at self
 * @author baselalmohamad
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket productApi() {

	return new Docket(DocumentationType.SWAGGER_2)
			.select()
			.apis(RequestHandlerSelectors.basePackage("com.SB")).build()
			.apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {


		return new ApiInfo("BOOTSPRING App Rest API", "this is a description", "versionNo",
				"terms-Of-ServiceUrl", new Contact("Basel Almohamad", "www.example.com", "abc@gmail.com"), "license of API","API licence URL", Collections.emptyList());
	}

}
