package com.SB.Repositrys;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.SB.model.Department;
import com.SB.model.EmployeePOJO;

/**
 * @param EmployeePOJO is the Name of your class ,which you wannt to handle
 * @Integer ist the Datatype of id in the Class
 * @author baselalmohamad
 *
 */
@Repository
public interface EmployeeRepositry extends JpaRepository<EmployeePOJO, Integer>{

	//Hier you can can write your custom method as query, the name of method represent the query statement
	//findBy+Attributes name
public List<EmployeePOJO> findByFirstNameIgnoreCase(String firstName);

//public List<EmployeePOJO> findByDepartment(Department deparment);// return all employees under this department





//public List<EmployeePOJO> findByFirstName(String firstName);
//public List<EmployeePOJO> findByFirstNameIgnoreCase(String firstName);


/* List<Person> findByEmailAddressAndLastname(EmailAddress emailAddress, String lastname);

  // Enables the distinct flag for the query
  List<Person> findDistinctPeopleByLastnameOrFirstname(String lastname, String firstname);
  List<Person> findPeopleDistinctByLastnameOrFirstname(String lastname, String firstname);

  // Enabling ignoring case for an individual property
  List<Person> findByLastnameIgnoreCase(String lastname);
  // Enabling ignoring case for all suitable properties
  List<Person> findByLastnameAndFirstnameAllIgnoreCase(String lastname, String firstname);

  // Enabling static ORDER BY for a query
  List<Person> findByLastnameOrderByFirstnameAsc(String lastname);
  List<Person> findByLastnameOrderByFirstnameDesc(String lastname);*/

}
