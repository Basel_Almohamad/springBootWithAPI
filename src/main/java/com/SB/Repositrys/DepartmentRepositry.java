package com.SB.Repositrys;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.SB.model.Department;

@Repository
public interface DepartmentRepositry extends JpaRepository<Department, Integer>{

}
